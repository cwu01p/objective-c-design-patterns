//
//  BuilderTests.m
//  DesignPatternsTests
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ActorBuilder.h"

@interface BuilderTests : XCTestCase

@end

@implementation BuilderTests

- (void)testExample {
    Actor *actor = [ActorBuilder makeActorWithBuilder:^(ActorBuilder *builder) {
        builder.firstName = @"Jose";
        builder.lastName = @"Rizal";
    }];
    
    XCTAssert([actor.firstName isEqualToString:@"Jose"], @"name should be Jose");
    XCTAssert([actor.lastName isEqualToString:@"Rizal"], @"name should be Rizal");
    XCTAssert([actor.fullName isEqualToString:@"Jose Rizal"], @"name should be Jose Rizal");
}

@end
