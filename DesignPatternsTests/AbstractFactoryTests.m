//
//  AbstractFactoryTests.m
//  DesignPatternsTests
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CharacterFactory.h"
#import "Warrior.h"
#import "Priest.h"

@interface AbstractFactoryTests : XCTestCase

@end

@implementation AbstractFactoryTests

- (void)testWarrior {
    CharacterFactory *factory = [CharacterFactory characterFactoryOfType:CharacterTypeWarrior];
    Character *character = [factory createCharacter];
    XCTAssert([character isKindOfClass:[Warrior class]], @"warrior should be of Warrior class.");
}

- (void)testPriest {
    CharacterFactory *factory = [CharacterFactory characterFactoryOfType:CharacterTypePriest];
    Character *character = [factory createCharacter];
    XCTAssert([character isKindOfClass:[Priest class]], @"warrior should be of Priest class.");
}

@end
