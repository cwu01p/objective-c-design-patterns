//
//  FactoryMethodTests.m
//  DesignPatternsTests
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Food.h"

@interface FactoryMethodTests : XCTestCase

@end

@implementation FactoryMethodTests

- (void)testExample {
    Food *food = [Food food];
    XCTAssert(food.name == nil);
    
    Food *apple = [Food foodWithName:@"Apple"];
    XCTAssert([apple.name isEqualToString:@"Apple"]);
    
    Food *anotherApple = [[Food alloc] init];
    anotherApple.name = @"Apple";
    XCTAssert([apple.name isEqualToString:anotherApple.name]);
}

@end
