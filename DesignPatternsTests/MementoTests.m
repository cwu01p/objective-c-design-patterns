//
//  MementoTests.m
//  DesignPatternsTests
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Memento.h"
#import "Originator.h"
#import "CareTaker.h"

@interface SalesProspect : NSObject <NSCopying>
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *phone;
@property (nonatomic) double budget;
@end

@implementation SalesProspect

- (id)copyWithZone:(NSZone *)zone {
    SalesProspect* copy = [[SalesProspect alloc] init];
    copy.name = self.name;
    copy.phone = self.phone;
    copy.budget = self.budget;
    return copy;
}

@end

@interface MementoTests : XCTestCase

@end

@implementation MementoTests

- (void)testExample {
    SalesProspect *salesProspect = [[SalesProspect alloc] init];
    salesProspect.name = @"Jose Rizal";
    salesProspect.phone = @"(01) 230 1896";
    salesProspect.budget = 851904000;
    
    XCTAssert([salesProspect.name isEqualToString:@"Jose Rizal"]);
    XCTAssert([salesProspect.phone isEqualToString:@"(01) 230 1896"]);
    XCTAssert(salesProspect.budget == 851904000);
    
    Originator<SalesProspect *> *originator = [[Originator alloc] initWithInitialState:salesProspect];
    
    // Store internal state
    Memento *memento = [originator saveMemento];
    CareTaker<SalesProspect *> *careTaker = [[CareTaker alloc] initWithMemento:memento];
    
    // Continue changing originator state
    salesProspect.name = @"Andres Bonifacio";
    salesProspect.phone = @"(00) 510 1897";
    salesProspect.budget = 863222400;
    
    XCTAssert([salesProspect.name isEqualToString:@"Andres Bonifacio"]);
    XCTAssert([salesProspect.phone isEqualToString:@"(00) 510 1897"]);
    XCTAssert(salesProspect.budget == 863222400);
    
    // Restore memento
    salesProspect = [originator restoreMemento:careTaker.memento];
    
    XCTAssert([salesProspect.name isEqualToString:@"Jose Rizal"]);
    XCTAssert([salesProspect.phone isEqualToString:@"(01) 230 1896"]);
    XCTAssert(salesProspect.budget == 851904000);
}

@end
