//
//  SingletonTests.m
//  DesignPatternsTests
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Singleton.h"

@interface SingletonTests : XCTestCase
@property (weak, nonatomic) Singleton *singleton;
@end

@implementation SingletonTests

- (void)setUp {
    [super setUp];
    
    self.singleton = [Singleton sharedInstance];
}

- (void)testExample {
    Singleton *anotherSingleton = [Singleton sharedInstance];
    
    XCTAssertEqual(self.singleton, anotherSingleton);
}

@end
