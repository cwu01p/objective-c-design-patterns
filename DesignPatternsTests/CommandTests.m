//
//  CommandTests.m
//  DesignPatternsTests
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PlumberFixLeakCommand.h"

@interface CommandTests : XCTestCase

@end

@implementation CommandTests

- (void)testExample {
    Plumber *plumber = [[Plumber alloc] init];
    PlumberFixLeakCommand *command = [[PlumberFixLeakCommand alloc] initWithPlumber:plumber];
    id fixedLeak = [command execute];
    
    XCTAssert([fixedLeak boolValue] == YES, @"Failed to fix leak");
}

@end
