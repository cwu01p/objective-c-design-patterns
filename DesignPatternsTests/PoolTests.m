//
//  PoolTests.m
//  DesignPatternsTests
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ObjectPool.h"

@interface PoolTests : XCTestCase

@end

@implementation PoolTests

- (void)testExample {
    ObjectPool<NSObject *> *pool = [[ObjectPool alloc] initWithCount:5 initBlock:^id{
        return [[NSObject alloc] init];
    }];
    
    XCTAssert(pool.allObjects.count == 5, @"pooled objects sould be 5.");
    
    NSObject *borrowedObject = [pool borrowObject];
    XCTAssert(borrowedObject != nil, @"borrowedObject cannot be nil.");
    XCTAssert(pool.allObjects.count == 4, @"pooled objects sould be 4.");
    
    [pool returnObjectToPool:borrowedObject];
    XCTAssert(pool.allObjects.count == 5, @"pooled objects sould be 5.");
}

@end
