//
//  Command.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Command <NSObject>

- (id)execute;

@end
