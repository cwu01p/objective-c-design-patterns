//
//  Plumber.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "Plumber.h"

@implementation Plumber

- (BOOL)fixLeak {
    NSLog(@"Fixing leak...");
    NSLog(@"Fixed leak.");
    return YES;
}

@end
