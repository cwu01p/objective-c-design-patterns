//
//  Plumber.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Plumber : NSObject

- (BOOL)fixLeak;

@end
