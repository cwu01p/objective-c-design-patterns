//
//  PlumberFixLeakCommand.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "PlumberFixLeakCommand.h"

@interface PlumberFixLeakCommand()
@property (strong, nonatomic) Plumber *plumber;
@end

@implementation PlumberFixLeakCommand

- (instancetype)init {
    return nil;
}

- (instancetype)initWithPlumber:(Plumber *)plumber {
    self = [super init];
    self.plumber = plumber;
    return self;
}

- (id)execute {
    return @([self.plumber fixLeak]);
}

@end
