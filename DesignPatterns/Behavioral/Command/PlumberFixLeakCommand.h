//
//  PlumberFixLeakCommand.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Command.h"
#import "Plumber.h"

@interface PlumberFixLeakCommand : NSObject <Command>

- (instancetype)initWithPlumber:(Plumber *)plumber;

@end
