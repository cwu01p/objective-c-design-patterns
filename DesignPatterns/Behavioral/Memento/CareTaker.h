//
//  CareTaker.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Memento.h"

@interface CareTaker<ObjectType:id<NSCopying>> : NSObject

@property (nonatomic, readonly) Memento<ObjectType> *memento;

- (instancetype)initWithMemento:(Memento<ObjectType> *)memento;

@end
