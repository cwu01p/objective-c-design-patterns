//
//  Memento.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Memento<ObjectType:id<NSCopying>> : NSObject

@property (readonly) ObjectType state;

- (instancetype)initWithState:(ObjectType)state;

@end
