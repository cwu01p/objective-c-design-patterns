//
//  Originator.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Memento.h"

@interface Originator<ObjectType:id<NSCopying>> : NSObject

- (instancetype)initWithInitialState:(ObjectType)state;
- (Memento<ObjectType> *)saveMemento;
- (ObjectType)restoreMemento:(Memento<ObjectType> *)memento;

@end
