//
//  Memento.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "Memento.h"

@interface Memento()
@property (copy, nonatomic, readwrite) id<NSCopying> state;
@end

@implementation Memento

- (instancetype)init {
    return nil;
}

- (instancetype)initWithState:(id)state {
    self = [super init];
    self.state = state;
    return self;
}

@end
