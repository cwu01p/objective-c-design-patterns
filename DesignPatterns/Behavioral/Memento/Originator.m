//
//  Originator.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "Originator.h"

@interface Originator()
@property (copy, nonatomic) id<NSCopying> state;
@end

@implementation Originator

- (instancetype)init {
    return nil;
}

- (instancetype)initWithInitialState:(id<NSCopying>)state {
    self = [super init];
    self.state = state;
    return self;
}

- (Memento *)saveMemento {
    return [[Memento alloc] initWithState:self.state];
}

- (id)restoreMemento:(Memento *)memento {
    self.state = memento.state;
    return self.state;
}

@end
