//
//  CareTaker.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "CareTaker.h"

@interface CareTaker()
@property (strong, nonatomic, readwrite) Memento *memento;
@end

@implementation CareTaker

- (instancetype)init {
    return nil;
}

- (instancetype)initWithMemento:(Memento *)memento {
    self = [super init];
    self.memento = memento;
    return self;
}

@end
