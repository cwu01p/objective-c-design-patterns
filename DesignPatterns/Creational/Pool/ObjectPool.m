//
//  ObjectPool.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "ObjectPool.h"

@interface ObjectPool()
@property (strong, nonatomic) InitBlock initBlock;
@property (strong, nonatomic) NSMutableArray *pool;
@end

@implementation ObjectPool

- (instancetype)initWithCount:(NSUInteger)count initBlock:(InitBlock)initBlock {
    self = [self init];
    
    self.initBlock = initBlock;
    
    self.pool = [NSMutableArray arrayWithCapacity:count];
    for (NSUInteger i = 0; i < count; i++) {
        id anObject = self.initBlock();
        [self.pool addObject:anObject];
    }
    
    return self;
}

- (id)borrowObject {
    @synchronized(self) {
        id borrowedObject = [self.pool lastObject];
        [self.pool removeLastObject];
        
        if (borrowedObject == nil) {
            borrowedObject = self.initBlock();
        }
        
        return borrowedObject;
    }
}

- (void)returnObjectToPool:(id)anObject {
    [self.pool addObject:anObject];
}

- (NSArray *)allObjects {
    return self.pool.copy;
}

@end
