//
//  ObjectPool.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ObjectPool<ObjectType> : NSObject

typedef ObjectType (^InitBlock)(void);

@property (nonatomic, readonly) NSArray<ObjectType> *allObjects;

- (instancetype)initWithCount:(NSUInteger)count initBlock:(InitBlock)initBlock;
- (ObjectType)borrowObject;
- (void)returnObjectToPool:(ObjectType)anObject;

@end

NS_ASSUME_NONNULL_END
