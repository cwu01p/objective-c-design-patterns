//
//  CharacterFactory.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Character.h"

typedef NS_ENUM(NSInteger, CharacterType) {
    CharacterTypeWarrior,
    CharacterTypePriest
};

@interface CharacterFactory : NSObject

+ (instancetype)characterFactoryOfType:(CharacterType)characterType;
- (Character *)createCharacter;

@end
