//
//  WarriorFactory.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "WarriorFactory.h"
#import "Warrior.h"

@implementation WarriorFactory

- (Character *)createCharacter {
    return [[Warrior alloc] init];
}

@end
