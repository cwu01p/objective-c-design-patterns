//
//  CharacterFactory.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "CharacterFactory.h"
#import "WarriorFactory.h"
#import "PriestFactory.h"

@implementation CharacterFactory

+ (instancetype)characterFactoryOfType:(CharacterType)characterType {
    switch (characterType) {
        case CharacterTypeWarrior:
            return [[WarriorFactory alloc] init];
        case CharacterTypePriest:
            return [[PriestFactory alloc] init];
    }
    
    return nil;
}

- (Character *)createCharacter {
    NSString *exceptionName = NSInternalInconsistencyException;
    NSString *exceptionReason = [NSString stringWithFormat:@"-[%@ createCharacter] was not implemented.", [self class]];
    @throw [NSException exceptionWithName:exceptionName reason:exceptionReason userInfo:nil];
}

@end
