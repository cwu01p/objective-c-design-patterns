//
//  PriestFactory.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "CharacterFactory.h"

@interface PriestFactory : CharacterFactory

@end
