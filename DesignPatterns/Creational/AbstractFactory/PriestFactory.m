//
//  PriestFactory.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "PriestFactory.h"
#import "Priest.h"

@implementation PriestFactory

- (Character *)createCharacter {
    return [[Priest alloc] init];
}

@end
