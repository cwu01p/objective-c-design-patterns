//
//  ActorBuilder.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "ActorBuilder.h"
#import "Actor+Builder.h"

@implementation ActorBuilder

+ (Actor *)makeActorWithBuilder:(void (^)(ActorBuilder *))builder {
    ActorBuilder *actorBuilder = [ActorBuilder new];
    builder(actorBuilder);
    return [[Actor alloc] initWithBuilder:actorBuilder];
}

@end
