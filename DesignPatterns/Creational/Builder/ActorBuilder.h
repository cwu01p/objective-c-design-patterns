//
//  ActorBuilder.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Actor.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActorBuilder : NSObject

@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;

+ (Actor *)makeActorWithBuilder:(void(^)(ActorBuilder *builder))builder;

@end

NS_ASSUME_NONNULL_END
