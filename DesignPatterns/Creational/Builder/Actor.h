//
//  Actor.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Actor : NSObject

@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (nonatomic, readonly) NSString *fullName;

@end

NS_ASSUME_NONNULL_END
