//
//  Actor+Builder.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "Actor.h"
#import "ActorBuilder.h"

NS_ASSUME_NONNULL_BEGIN

@interface Actor (Builder)

- (instancetype)initWithBuilder:(ActorBuilder *)builder;

@end

NS_ASSUME_NONNULL_END
