//
//  Actor+Builder.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "Actor+Builder.h"

@implementation Actor (Builder)

- (instancetype)initWithBuilder:(ActorBuilder *)builder {
    self = [self init];
    self.firstName = builder.firstName;
    self.lastName = builder.lastName;
    return self;
}

@end
