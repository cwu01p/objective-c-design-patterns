//
//  Food.m
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import "Food.h"


@implementation Food

+ (instancetype)food {
    return [[self alloc] init];
}

+ (instancetype)foodWithName:(NSString *)name {
    Food *food = [self food];
    food.name = name;
    return food;
}

@end
