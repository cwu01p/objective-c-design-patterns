//
//  Food.h
//  DesignPatterns
//
//  Copyright © 2018 CWU-01P. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Food : NSObject

@property (copy, nonatomic) NSString *name;

+ (instancetype)food;
+ (instancetype)foodWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
